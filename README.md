# Deployment de mlflow en CloudRun
Esta guía proporciona instrucciones detalladas para implementar MLflow en Google Cloud Platform (GCP) utilizando Cloud Run, Cloud SQL y Cloud Storage. Al seguir los pasos descritos en esta documentación, podrás configurar un servidor MLflow escalable y seguro que se comunica con una base de datos PostgreSQL en Cloud SQL y almacena artefactos de experimentos en un bucket de Cloud Storage.

## Arquitectura

El proyecto estará configurado de la siguiente manera:

![graphic](images/graphic.png)

- **Cloud Run:** En el proyecto, Cloud Run se utiliza para implementar el servidor MLflow. La principal ventaja de usar Cloud Run es que se encarga automáticamente de la escalabilidad, el aprovisionamiento y la administración de los recursos necesarios para ejecutar tu aplicación.
- **Cloud VPC:** Es una red privada virtual en Google Cloud. En este proyecto, la Cloud VPC permite la comunicación segura entre Cloud Run y Cloud SQL. Al configurar una VPC, puedes controlar el acceso a los recursos y establecer reglas de firewall para proteger tus servicios. La conexión VPC entre Cloud Run y Cloud SQL asegura que el tráfico entre ellos permanezca en la red privada de Google Cloud.
- **Cloud SQL:** Es un servicio de base de datos totalmente administrado en Google Cloud que facilita la configuración, el mantenimiento, la administración y la supervisión de bases de datos relacionales en la nube. En este proyecto, se utiliza Cloud SQL para alojar la base de datos PostgreSQL que almacena los metadatos de MLflow, como experimentos, ejecuciones y parámetros.
- **Cloud Storage:** Es un servicio de almacenamiento en la nube de Google Cloud que permite guardar y acceder a datos de manera segura y duradera. En el proyecto, Cloud Storage se utiliza como el "artifact store" de MLflow, lo que significa que los archivos de resultados y modelos generados por tus experimentos de ML se almacenan en un bucket de Cloud Storage. Esto permite un fácil acceso y compartición de los artefactos generados por tus experimentos de Machine Learning.

# Proceso para realizar el deploy de mlflow en **Cloud Run**

## 1. Crear los archivos necesarios

Usando el Cloud Shell vamos a crear los siguientes archivos

entrypoint.sh:

```
#!/bin/sh
mlflow server \
  --host 0.0.0.0 \
  --port 8080 \
  --backend-store-uri "${MLFLOW_BACKEND_STORE_URI}" \
  --default-artifact-root "${MLFLOW_DEFAULT_ARTIFACT_ROOT}" \
  --workers 4 \
  --expose-prometheus /tmp
```

Dockerfile:

```
# Use Python 3.8 slim image as base
FROM python:3.8-slim

# Set working directory to /app
WORKDIR /app

# Copy requirements.txt into the container
COPY requirements.txt .

# Install required packages using requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Copy entrypoint.sh into the container
COPY entrypoint.sh .

# Copy service account key JSON into the container
# COPY credentials.json /app/credentials.json

# Set GOOGLE_APPLICATION_CREDENTIALS environment variable
# ENV GOOGLE_APPLICATION_CREDENTIALS=/app/credentials.json

# Expose port 8080
EXPOSE 8080

# Set entrypoint
ENTRYPOINT ["./entrypoint.sh"]
```


requirements.txt

```
mlflow
gunicorn
google-cloud-storage
prometheus-flask-exporter
psycopg2-binary
```

## 2. Build & push para **Google Container Registry**

Primero damos permisos de ejecución al entrypoint.sh con

```bash
chmod +x entrypoint.sh
```

Luego haremos el build & push para el Container Registry, reemplazamos el <PROJECT_ID> por el ID de nuestro proyecto de GCP.

```bash
docker build -t gcr.io/<PROJECT_ID>/mlflow-server:latest .
docker push gcr.io/<PROJECT_ID>/mlflow-server:latest
```

## 3. Crear las bases de datos para **MLflow**

Configuración de los dos almacenamientos en Google Cloud Platform (GCP) para usar con MLflow: 

- Almacenamiento de backend (para almacenar información sobre experimentos y métricas)
- Raíz de artefactos por defecto (para almacenar artefactos como modelos y datos).

1. **Almacenamiento de backend (Cloud SQL):**
    
    Puedes utilizar Cloud SQL como tu almacenamiento de backend para MLflow. A continuación, te muestro cómo crear una instancia de Cloud SQL con PostgreSQL:
    
    a. Ve al **[Cloud SQL en la consola de Google Cloud](https://console.cloud.google.com/sql/)**.
    
    b. Haz clic en "Crear instancia" y selecciona "PostgreSQL".
    
    c. Completa los detalles de la instancia, como el ID de la instancia, la contraseña del usuario 'postgres' y la región.
    
    d. Haz clic en "Crear" para crear la instancia de Cloud SQL.
    
    e. Una vez creada la instancia, ve a la pestaña "Usuarios" y crea un nuevo usuario para MLflow con los privilegios necesarios.
    
    f. Para obtener la **`<BACKEND_STORE_URI>`**, usa el siguiente formato: **`postgresql://<USERNAME>:<PASSWORD>@<INSTANCE_IP>:5432/<DB_NAME>`**. Reemplaza **`<USERNAME>`**, **`<PASSWORD>`**, **`<INSTANCE_IP>`** y **`<DB_NAME>`** con la información correspondiente de tu instancia de Cloud SQL
    (El instance IP para este caso será el ip privado).
    
2. **Raíz de artefactos por defecto (Cloud Storage):**
    
    Para configurar un bucket en Cloud Storage como la raíz de artefactos por defecto, sigue estos pasos:
    
    a. Ve al **[Cloud Storage en la consola de Google Cloud](https://console.cloud.google.com/storage/)**.
    
    b. Haz clic en "Crear bucket".
    
    c. Ingresa un nombre único para el bucket y selecciona la ubicación y opciones de almacenamiento de acuerdo con tus necesidades.
    
    d. Haz clic en "Crear" para crear el bucket.
    
    e. Para obtener la **`<DEFAULT_ARTIFACT_ROOT>`**, usa el siguiente formato: **`gs://<BUCKET_NAME>`**. Reemplaza **`<BUCKET_NAME>`** con el nombre del bucket que acabas de crear.
    

## 4. Hacer el deploy en **Cloud Run**

Vamos a realizar el deploy con el siguiente comando

- **image:** la imagen que se encuentra en Container Registry
- **set-env-vars:** instanciamos las variables de entorno con las direcciones obtenidas en el paso anterior
- **region:** usaremos el us-east1 para CMPC
- **memory:** vamos a configurarlo en 1Gi ya que los 0.5Gi que da por defecto no es suficiente para correr el mlflow.

```bash
gcloud run deploy mlflow-server  \
 --image gcr.io/<PROJECT_ID>/mlflow-server:latest \
 --allow-unauthenticated \
 --set-env-vars "MLFLOW_BACKEND_STORE_URI=**<BACKEND_STORE_URI>**,MLFLOW_DEFAULT_ARTIFACT_ROOT=**<DEFAULT_ARTIFACT_ROOT>**"  \
 --region us-east1  \
 --platform managed \
 --port 8080 \
 --memory 1Gi
```


**Este deploy va a tirar error ya que no va a ser capaz de conectarse a la base de datos pero se va a crear la instancia en CloudRun, en el siguiente paso se muestra cómo conectar el CloudRun y el Cloud SQL con un VPC.**

## 5. Conectar el **Cloud Run** con el **Cloud SQL** a través del **Cloud VPC**

Vamos a conectar el CloudRun con el Cloud SQL a través de Cloud VPN para tener una conexión privada y no exponer la base de datos a una ip pública.

información sobre costos de un VPC en GCP: [Precios | Nube privada virtual | Google Cloud](https://cloud.google.com/vpc/pricing?hl=es-419)

### Configurar el **Cloud VCP**

1. Vamos al VCP network en Google Cloud Platform
2. Entramos a la pestaña “Serverless VPC access” y le damos a “Create connector”.
3. Elegimos un nombre y en región tenemos que poner la misma que seleccionamos en nuestro Cloud SQL.
4. En Subnet elegimos “Custom IP range” y escribimos 10.8.0.0

### Configurar el **Cloud SQL**

1. Vamos al Cloud SQL , seleccionamos la instancia y le damos a editar.
2. En la parte “Connections” desseleccionamos Public IP y seleccionamos Private IP.
3. En Network elegimos default.

### Configurar el **Cloud Run**

1. Vamos al Cloud Run, seleccionamos nuestro deploy y le damos a editar.
2. En la pestaña Connections, vamos hasta la parte de VPC Connector y seleccionamos el conector que creamos anteriormente en el Cloud VCP
3. Le damos clic a Deploy.

## Conclusión y siguientes pasos

Con este proceso ya tendríamos el mlflow corriendo en Cloud Run y este conectado a las bases de datos, los siguientes pasos sería administrar el auth para que sólo puedan entrar las personas autorizadas e investigar cómo sería el tema del dominio personalizado para la url.